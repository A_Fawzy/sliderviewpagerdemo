package android.example.com.sliderpagertest.adapter

import android.content.Context
import android.example.com.sliderpagertest.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class SliderPagerAdapter(private val mContext: Context, private val items: List<String>)
    :RecyclerView.Adapter<SliderPagerAdapter.ViewHolder>(){
    
    private var customPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = LayoutInflater.from(mContext)
            .inflate(R.layout.slider_list_item, parent,false)
        
        return ViewHolder(rootView)
    }

    override fun getItemCount(): Int{
        return Int.MAX_VALUE
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        customPosition++
        if(customPosition > items.count()-1)
            customPosition = 0
        val itemUrl = items[customPosition]
        holder.bindImageView(itemUrl)
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        private val imageView = itemView.findViewById<ImageView>(R.id.imageView)

        fun bindImageView(url: String){
            imageView.loadFromUrl(url)
        }
    }
}

fun ImageView.loadFromUrl(url: String){
    Picasso
        .get()
        .load(url)
        .into(this)
}