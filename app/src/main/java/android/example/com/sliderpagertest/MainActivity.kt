package android.example.com.sliderpagertest

import android.example.com.sliderpagertest.adapter.SliderPagerAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import androidx.viewpager2.widget.ViewPager2
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private var adapter: SliderPagerAdapter? = null
    private var mViewPager2: ViewPager2? = null
    private var currentImagePosition = 0
    private var isAutoSliding = false
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        val items = ArrayList<String>()
        val button = findViewById<Button>(R.id.button)
        items.add("https://images.unsplash.com/photo-1543185377-b75671ac8741?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80")
        items.add("https://images.unsplash.com/photo-1517404215738-15263e9f9178?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80")
        items.add("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png")
        items.add("https://images.unsplash.com/photo-1517404215738-15263e9f9178?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80")
        items.add("https://images.unsplash.com/photo-1543185377-b75671ac8741?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80")
        items.add("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png")
        
        adapter = SliderPagerAdapter(this, items)
        mViewPager2 = findViewById(R.id.viewPager2)
        mViewPager2?.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        mViewPager2?.adapter = adapter
        startAutoSlider()
        button.setOnClickListener {
            if(isAutoSliding) {
                isAutoSliding = false
                timer?.purge()
                timerTask?.cancel()
            }
            else{
                startAutoSlider()
            }
        }
    }
    
    private fun startAutoSlider() {
        isAutoSliding = true
        val handler = Handler()
        val runnable = Runnable {
            currentImagePosition++
            if (currentImagePosition == Int.MAX_VALUE)
                currentImagePosition = 0
            mViewPager2?.setCurrentItem(currentImagePosition, true)
        }
        timer = Timer()
        timerTask = object: TimerTask(){
            override fun run() {
                handler.post(runnable)
            }
    
        }
        timer?.schedule(timerTask, 250, 3000)
    }
}
